/*
 * Copyright (C) 2016, DatArcs ltd.
 * Author: Andrey Gelman <andrey.gelman@datarcs.com>
 * License: GNU GPLv2+
 */

#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/syscall.h>

#include "heartbeats.h"


static inline pid_t gettid(void)
{
	return syscall( __NR_gettid );
}

/* client */
UDPChannel *HB_initialize_client(const char *server_ip, unsigned int portno)
{
	UDPChannel *p;
	int addr_valid;

	p = calloc(1, sizeof(UDPChannel));

	p->sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (p->sd < 0) {
		goto init_err_0;
	}

	p->si_remote.sin_family = AF_INET;
	p->si_remote.sin_port = htons(portno);
	addr_valid = inet_aton(server_ip, &p->si_remote.sin_addr);
	if (!addr_valid) {
		goto init_err_0;
	}

	return p;

init_err_0:
	free(p);
	return NULL;
}

int HB_set_target_rate(UDPChannel *p, int win, int min, int max, bool local)
{
	int n;
	HB_Message message;

	message.mtype = HB_MTYPE_TARGET_RATE;
	message.local = local;
	message.rate.winsize = win;
	message.rate.min     = min;
	message.rate.max     = max;

	n = sendto(p->sd, &message, sizeof(message), 0, (struct sockaddr *)&p->si_remote, sizeof(p->si_remote));
	return n;
}

int HB_heartbeat(UDPChannel *p, int tag, bool local)
{
	int n;
	HB_Message message;

	message.mtype = HB_MTYPE_HEARTBEAT;
	message.local = local;
	message.heartbeat.tag       = tag;
	clock_gettime(CLOCK_MONOTONIC_RAW, &message.heartbeat.timestamp);
	message.heartbeat.thread_id = gettid();

	n = sendto(p->sd, &message, sizeof(message), 0, (struct sockaddr *)&p->si_remote, sizeof(p->si_remote));
	return n;
}

