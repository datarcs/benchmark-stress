/*
 * Copyright (C) 2016, DatArcs ltd.
 * Author: Andrey Gelman <andrey.gelman@datarcs.com>
 * License: GNU GPLv2+
 *
 * Heartbeats is a (client/server-based) infrastructure for measuring
 * application performance.
 * Heartbeats client side requires Heartbeats-instrumented application.
 * Reference:
 * [1] Application Heartbeats for Software Performance and Health.
 *     H. Hoffmann, J. Eastep, M. Santambrogio, J. Miller, A. Agarwal
 *     MIT-CSAIL 2009
 */

#ifndef _HEARTBEATS_H
#define _HEARTBEATS_H

#include <time.h>
#include <stdbool.h>
#include <netinet/in.h>


#define HB_DEFAULT_UDP_PORT		8008


typedef struct {
	int sd;
	struct sockaddr_in si_remote;
} UDPChannel;

enum {
	HB_MTYPE_TARGET_RATE = 0xDA7A8C50,
	HB_MTYPE_HEARTBEAT   = 0xDA7A8C51,
};

typedef struct {
	int winsize;
	int min;
	int max;
} HB_TargetRate;

typedef struct {
	int tag;
	struct timespec timestamp;
	long thread_id;
} HB_Heartbeat;

typedef struct {
	unsigned int mtype;
	bool local;
	union {
		HB_TargetRate rate;
		HB_Heartbeat heartbeat;
	};
} HB_Message;


#define HB_heartbeat_once_in(ratio, p, tag, local)	\
	do {						\
		static int __HB_count = 0;		\
		if (__HB_count <= 0)			\
			__HB_count = (ratio);		\
		if (--__HB_count > 0)			\
			break;				\
		HB_heartbeat((p), (tag), (local));	\
	} while (0)


/*
 * Common arguments:
 * - local -- thread-local vs. application-wide heartbeat
 */

/* client */
UDPChannel *HB_initialize_client(const char *server_ip, unsigned int portno);

/*
 * Arguments:
 * - win -- number of heartbeats used to calculate the moving average
 * - min -- minimum required heartbeat rate
 * - max -- maximum required heartbeat rate
 */
int HB_set_target_rate(UDPChannel *p, int win, int min, int max, bool local);

/*
 * Arguments:
 * - tag -- general purpose info (e.g. P-frame, I-frame ...)
 * Each heartbeat is stamped with:
 * - current time
 * - thread ID
 */
int HB_heartbeat(UDPChannel *p, int tag, bool local);

/* server */
#ifdef TBD_HB_SERVER
void HB_initialize_server();
int HB_current_rate(int win, bool local);
int HB_get_target_min(bool local);
int HB_get_target_max(bool local);
int *HB_get_history(int n);
#endif
#endif	/* _HEARTBEATS_H */

